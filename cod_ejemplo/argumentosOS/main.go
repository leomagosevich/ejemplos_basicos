package main

import (
	"fmt"
	"os"
)

func main() {

	//Se ejecuta:
	//go run main.go $MYSQL_DB o ./main $MYSQL_DB
	//os.Args[0] corresponde al nombre del programa
	//La salida será: El nombre de la BD es rcs_db

	db := os.Args[1]

	fmt.Printf("El nombre de la BD es %s\n", db)
}
