package main

import "fmt"

type Person struct {
	Name string
}

func (p *Person) Introduce() {
	fmt.Printf("Hola, yo soy %s\n", p.Name)
}

type Saiyan struct {
	*Person
	Power int
}

func (s *Saiyan) Introduce() {
	fmt.Printf("Hola, yo soy %s y mi poder es de %d\n", s.Name, s.Power)
}

func main() {
	goku := &Saiyan{
		Person: &Person{"Goku"},
		Power:  9001,
	}

	// Invoco la función sobrecargada
	goku.Introduce()
	// Ejecuto la función original
	goku.Person.Introduce()
}
