package main

import "fmt"

func main() {

    scores := make([]int, 0, 5)
    c := cap(scores)
    fmt.Printf("La capacidad inicial es \"%d\"\n", c) 
    j := 0
    long := len(scores)
    fmt.Printf("La longitud inicial es \"%d\"\n", long)

    for i := 0; i < 25; i++ {
        scores = append(scores, i)
        
        // Si la capacidad cambia, Go tiene que hacer crecer el array para poder volcar los datos
        if cap(scores) != c {
            j = j+1
            c = cap(scores)
            fmt.Printf("Expansion %d, ahora la capacidad es \"%d\"\n", j, c)
        }
    }
    long = len(scores)
    fmt.Printf("La longitud final es \"%d\"\n", long)
    fmt.Println("El contenido del slice scores:", scores)
}
