package main

import (
	"fmt"
	"shopping"
)

func main()  {
	var itemId int
	
	fmt.Println("Ingresa un ID de producto:")
	fmt.Scanln(&itemId)
	//fmt.Scanf("%d", &itemId)
	
	//precio, existe := shopping.PriceCheck(1000)
	precio, existe := shopping.PriceCheck(itemId)

	//fmt.Println(shopping.PriceCheck(9999))
	fmt.Printf("¿El ítem existe? %t\n", existe)

	if existe {
		fmt.Printf("El precio es %.3f\n", precio)
	}
}