package db

type Item struct {
	Price float64
}

// No se accede realmente a una BD, es solo un ejemplo de organizacion de código en package
// Creo una función que devuelve un valor puntual de un mapa
func LoadItem(id int) *Item {
	var precio = map[int] *Item {
		4343: &Item{
			9001,
		},
		4344: &Item{
			5436.98,
		},
		1001: &Item{
			201.45,
		},
		9999: &Item{
			40025211.21,
		},
	}
	
	return precio[id]
}