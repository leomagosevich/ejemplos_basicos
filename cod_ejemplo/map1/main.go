package main

import "fmt"

func main() {
	lookup := make(map[string] int, 10)
	lookup["Goku"] = 9001
	lookup["Gohan"] = 1001

	/*lookup := map[string] int{
		"Goku": 9001,
		"Gohan": 2044,
	}*/

	power, exists := lookup["Vegeta"]
	fmt.Println(power, exists)
	power, exists = lookup["Goku"]
	fmt.Println(power, exists)

	// Cantidad de pares clave-valor
	total := len(lookup)

	fmt.Println("Total:", total)

	fmt.Println("Voy a iterar")

	for key, value := range lookup {
		fmt.Println(key, "-", value)
	}
}