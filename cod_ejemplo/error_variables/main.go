package main

import (
	"errors"
	"fmt"
	"math"
)

// Detrmina el signo del parámetro recibido y le asigno 1 si es positivo, -1 si es negativo, 0 si es cero
func Sign(x float64) int {
	switch {
	case x > 0:
		return 1
	case x < 0:
		return -1
	default:
		return 0
	}
}

// Realiza la división de dos números y devuelve el resultado y un error
// En el ejemplo, se detecta si el resultado es erróneo por división por 0, en ese caso, se retorna un mensaje de error
// Para evaluar el error se utiliza la función IsInf del paquete math que recibe como parámetro un valor flotante a evaluar
// y un valor entero para determinar el signo (obtenid de la función Sing). Si el valor evaluado es infinito,
// IsInf retorna true, en caso contrario, false
func Dividir(dividendo float64, divisor float64) (float64, error) {
	resultado := dividendo / divisor

	signo := Sign(resultado)

	fmt.Println("El signo es:", signo)

	if math.IsInf(resultado, signo) {
		// Se retorna el error
		return resultado, errors.New("No se puede dividir por 0")
		//return resultado, errors.New("eel euq le otup")
	}
	// Se retorna el resultado, el error es nulo
	return resultado, nil

}

func main() {
	resultado, err := Dividir(-8, -2)
	//resultado := Dividir(8,0)

	if err == nil {
		fmt.Printf("El resultado es: %.3f\n", resultado)
	} else {
		fmt.Println("Error:", err)
		fmt.Println("Resultado:", resultado)
	}
}
