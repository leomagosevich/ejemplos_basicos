package main

import (
	"fmt"
	"os"
)

func main() {
	var mensaje string
	var err error
	//var f os.File

	fmt.Println("Ingresá lo que quieras escribir")

	fmt.Scanf("%s", &mensaje)

	//b := []byte(mensaje)

	nombre := "prueba.txt"

	//Si el archivo existe, se abre como solo lectura y para agregar contenido. Si no existe, se crea
	f, err := os.OpenFile(nombre, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0644)

	/*if err != nil {
		if _, err = os.Stat(nombre); os.IsNotExist(err){
			f,err := os.Create(nombre)
			if err != nil {
				fmt.Printf("Error: %v\n", err)
			}

		}
	}*/

	if err != nil {
		fmt.Printf("Error: %v\n", err)
	}

	defer f.Close()

	//_, err = f.Write(b)

	_, err = f.WriteString(mensaje + "\n")

	if err != nil {
		fmt.Printf("Error: %v\n", err)
	}

	f.Sync()

}
