package main

import (
	"fmt"
	"io/ioutil"
)

func main() {
	var mensaje string
	fmt.Println("Ingresá lo que quieras escribir")
	fmt.Scanf("%s", &mensaje)
	b := []byte(mensaje)
	err := ioutil.WriteFile("prueba.txt", b, 0644)
	if err != nil {
		fmt.Printf("Error: %v\n", err)
	}
}
