package main

import "fmt"

// Definición de la estructura
type Saiyan struct {
  Name string
  Power int
}

func (s *Saiyan) Super() {
  s.Power += 10000
}

func (s Saiyan) Super2() {
  s.Power += 10000
}

func main() {
  goku := &Saiyan{"Goku", 9001}
  vegeta := Saiyan{"Vegeta", 9000}
  goku.Super()
  vegeta.Super2()
  fmt.Println("Poder de Gokú:", goku.Power)
  fmt.Println("Poder de Vegeta:", vegeta.Power)
}
