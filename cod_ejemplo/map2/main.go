package main

import "fmt"

type Materias struct {
	nombre string
	creditos int
}

func main()  {
	materias := map[string] Materias {
		"COMP": Materias{
			"Compiladores",
			12,
		},
		"INTPROG": Materias{
			"Introduccion a la Programacion",
			6,	
		},
		"PROG1": Materias{
			"Programacion 1",
			8,
		},
		"PROG2": Materias{
			"Programacion 2",
			10,
		},
		"POO": Materias{
			"Programación Orientada a Objetos",
			10,
		},
		"TC1": Materias{
			"Trabajo de Campo 1",
			15,
		},
	}

	for materia := range materias {
		fmt.Println("Codigo:", materia, " | Nombre:", materias[materia].nombre, " | Creditos:", materias[materia].creditos)
	}

	fmt.Println()

	// Agrego una materia
	materias["SEM"] = Materias{
		"Seminario",
		15,
	}

	for materia := range materias {
		fmt.Println("Codigo:", materia, " | Nombre:", materias[materia].nombre, " | Creditos:", materias[materia].creditos)
	}

	fmt.Println()

	// Busco una materia por una clave específica
	mat, ok := materias["TC1"]

	if ok {
		fmt.Println("La materia es:", mat.nombre)
	} else {
		fmt.Println("La materia solicitada no existe")
	}
	
	fmt.Println()

	mat, ok = materias["TC2"]

	if ok {
		fmt.Println("La materia es:", mat.nombre)
	} else {
		fmt.Println("La materia solicitada no existe")
	}
}