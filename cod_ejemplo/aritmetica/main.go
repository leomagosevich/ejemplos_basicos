package main

import "fmt"

func miFuncion(parametro1 int32, parametro2 string) (float32, bool, string) {
    // Cuerpo de la función donde se realiza una acción específica
    // En el ejemplo, recibe dos parámetros, uno entero y otro flotante
    // y retorna tres valores: un número flotante, un booleano y una cadena de
    // texto. Para el retorno se utiliza la palabra clave return
    const pi = 3.21416
    var resultado float32 = pi * float32(parametro1)
    var soyBooleano bool = true
    var texto string = parametro2

    return resultado, soyBooleano, texto
}

func main()  {
    var a int32

    a = 2

    f, b, c := miFuncion(a, "esto es una cadena de texto")
    
    fmt.Printf("Esto lo retorna la función. f: %f - b: %t - s: %s\n", f, b, c)

}
