package main

import "fmt"

// Remueve un elemento, no respeta el orden
func removeAtIndex(source []int, index int) []int {
	lastIndex := len(source) - 1
	//intercambiamos el último valor con aquel que queremos eliminar
	source[index], source[lastIndex] = source[lastIndex], source[index]
	return source[:lastIndex]
}

func main() {
	scores := []int{1, 2, 3, 4, 5}
	scores = removeAtIndex(scores, 4)
	fmt.Println(scores)
}