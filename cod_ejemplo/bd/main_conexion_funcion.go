package main

import (
	"database/sql"
	"fmt"

	_ "github.com/go-sql-driver/mysql"
)

func conectarBD(usu string, pass string, host string, base string) (db *sql.DB, e error) {

	db, err := sql.Open("mysql", fmt.Sprintf("%s:%s@%s/%s", usu, pass, host, base))

	if err != nil {
		return nil, err
	}

	return db, nil

}

func main() {

	var (
		version string
		//resultado string
		usuario string
		passwd  string
		host    string
		base    string
		//usu     Usuario
	)

	fmt.Printf("usuario: ")
	fmt.Scanf("%s", &usuario)
	fmt.Printf("Contraseña: ")
	fmt.Scanf("%s", &passwd)
	fmt.Printf("Host indicar tcp(IP:PTO): ")
	fmt.Scanf("%s", &host)
	fmt.Printf("Base de Datos: ")
	fmt.Scanf("%s", &base)

	db, err := conectarBD(usuario, passwd, host, base)

	if err != nil {
		fmt.Printf("Error al conectar la BD: %v", err)
	}

	defer db.Close()

	db.QueryRow("select version()").Scan(&version)
	fmt.Println("La versión de la BD es:", version)

}
