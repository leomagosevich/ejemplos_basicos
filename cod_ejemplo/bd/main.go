package main

import (
	"database/sql"
	"fmt"

	_ "github.com/go-sql-driver/mysql"
)

func main() {
	var (
		version string
		usuario string
		passwd  string
		base    string
	)

	fmt.Printf("usuario: ")
	fmt.Scanf("%s", &usuario)
	fmt.Printf("Contraseña: ")
	fmt.Scanf("%s", &passwd)
	fmt.Printf("Base de Datos:")
	fmt.Scanf("%s", &base)
	
	//la conexión a la BD es "driver", "usario:passwd@/base"
	//db, _ := sql.Open("mysql", "root:Congreso3924@/mago")
	db, _ := sql.Open("mysql", fmt.Sprintf("%s:%s@/%s", usuario, passwd, base))
	defer db.Close()

	db.QueryRow("select version()").Scan(&version)
	fmt.Println("Maria DB version:", version)

}
