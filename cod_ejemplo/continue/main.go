package main

import "fmt"

func main() {
	i := 10

	for i < 20 {
		if i > 13 && i < 19 {
			i++
			continue
		}
		fmt.Println("El valor de i es", i)
		i ++
	}
}