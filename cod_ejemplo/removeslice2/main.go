package main

import "fmt"

// Remueve un elemento respetando el orden
func removeAtIndex(source[]int, index int) []int {
	lastIndex := len(source) - 1

	for i:= index + 1; i <= lastIndex; i++ {
		//intercambiamos de a uno los elementos desde el que quiero remover hasta el último
		source[i-1],source[i] = source[i], source[i-1]
	}
	return source[:lastIndex]
}

func main() {
	scores := []int{1, 2, 3, 4, 5}
	scores = removeAtIndex(scores, 0)
	fmt.Println(scores)
}