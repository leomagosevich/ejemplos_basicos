package main

import "fmt"

func main() {

	switch a, b := 10, -4; {
	case a > 0:
		fmt.Println("a es positivo")
		fallthrough
	case a < 0:
		fmt.Println("a es negativo")
		fallthrough
	case a == 0:
		fmt.Println("a es cero")
		fallthrough
	case b > 0:
		fmt.Println("b es positivo")
	case b < 0:
		fmt.Println("b es negativo")
	case b == 0:
		fmt.Println("b es cero")
	default:
		fmt.Println("No se pudo evaluar el signo de a y b")
	}

	switch a, b := 10, -4; {
	case a > b:
		fmt.Println("a es mayor que b")
	case a < b:
		fmt.Println("a es menor que b")
	default:
		fmt.Println("a y b iguales")
	}
}
