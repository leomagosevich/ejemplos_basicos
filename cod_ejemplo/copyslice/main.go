package main

import (
	"fmt"
	"math/rand"
	"sort"
	"time"
)

func main(){
	scores := make([]int, 40)
	// Go maneja números pseudoaleatorios, para que en cada ejecución los valores sean diferentes es necesario inicializar la función seed con algún valor variable
	// En en ejemplo, para inicializar la función Seed, se utiliza la función Now() de la hora del sistema
	rand.Seed(time.Now().Unix())
	for i := 1; i < len(scores); i++ {
		scores[i] = int(rand.Int31n(50))
	}

	fmt.Println(scores)
	sort.Ints(scores)
	fmt.Println(scores)

	worts := []int{1000, 1000, 1000, 1000, 1000}

	// Copio los primeros 5 elementos
	//copy(worts, scores[:5])

	// Copio los ultimos 5 elementos
	//copy(worts, scores[len(scores)-5:])
	
	// Copio un rango determinado a en un espacio determinado. Incluye indice inicial, excluye indice final.
	// Copia solo la cantidad de elementos que entran en el nuevo slice
	// Si se determina un rango en el origen, incluye indice inicial, excluye indice final
	//copy(worts[1:5], scores[2:4])
	copy(worts[2:4], scores[:10])

	fmt.Println(worts)
}