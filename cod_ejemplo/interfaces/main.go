package main

import (
	"fmt"
	"math"
)

type Geometrico interface {
	area() float64
	perimetro() float64
}

type Rectangulo struct {
	alto, ancho float64
}

type Circulo struct {
	radio float64
}

func (r Rectangulo) area() float64 {
	return r.alto * r.ancho	
}

func (r Rectangulo) perimetro() float64 {
	return 2 * r.alto + 2 * r.ancho
}

func (c Circulo) area() float64 {
	return math.Pi * math.Pow(c.radio, 2)
}
func (c Circulo) perimetro() float64 {
	return 2 * math.Pi * c.radio
}
func medidas (g Geometrico) {
	//fmt.Println("El área es:", g.area())
	fmt.Printf("El área es: %.3f\n", g.area())
	//fmt.Println("El perímetro es:", g.perimetro())
	fmt.Printf("El perímetro es: %.3f\n", g.perimetro())
}

//Funcion Principal
func main() {
	r := Rectangulo{
		alto: 3,
		ancho: 4,
	}

	c := Circulo{
		radio: 5,
	}

	fmt.Println("Rectángulo - Alto:", r.alto, " - Ancho:", r.ancho)
	medidas(r)
	fmt.Println()
	fmt.Println("Radio:", c.radio)
	medidas(c)
}
