package main

import "fmt"

func main() {
	i, j := 0, 0
loop:
	for {
		fmt.Println("Estoy en el primer loop e i vale", i)
		i += 1
		for {
			fmt.Println("Estoy en el segundo loop y j vale", j)
			if j == 3 {
				break loop
			}
			j += 1
		}
	}
	fmt.Println("Estoy fuera del for")
}
