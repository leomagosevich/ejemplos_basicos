package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	temp := make([]float32, 10)
	
	rand.Seed(time.Now().Unix())

	fmt.Println(time.Now().Unix())

	var multi int
	var multif float32

	multi = int(rand.Int31n(50))
	multif = float32(multi)

	//Hago que se generen valores negativos
	if multif < 25 {
		multif = multif * (-1)
	}

	fmt.Printf("Multi = %d\n", multi)
	fmt.Printf("Multif = %.2f\n", multif)

	for i := 0; i < len(temp); i++ {
		temp[i] = float32(rand.Float32() * multif)
		fmt.Println("El valor de la posición", i, "es", temp[i])
	}

	fmt.Println(temp)
}