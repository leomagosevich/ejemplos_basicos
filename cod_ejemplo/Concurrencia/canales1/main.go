package main

import (
	"fmt"
	"sync"
	"time"
)

var wg sync.WaitGroup

func IsReady(what string, seconds time.Duration, inv int, c chan int) {
	defer wg.Done()
	c <- inv
	//time.Sleep(minutes * 60 * 1e9)
	time.Sleep(seconds * time.Millisecond)
	fmt.Println(what, "está listo")
}

func main() {
	var invocacion int
	//var input string
	invocacion = 1
	ch := make(chan int)
	//ch <- 1
	wg.Add(2)
	go IsReady("Café", 10000, invocacion, ch)
	invo := <-ch
	fmt.Println("Invocación: ", invo)
	invocacion++
	//ch <- 2
	//wg.Add(1)
	go IsReady("Te", 5000, invocacion, ch)
	invo = <-ch
	fmt.Println("Invocación: ", invo)
	fmt.Println("Estoy esperando...")
	wg.Wait()
	//fmt.Scanln(&input)
	fmt.Println("Terminó")
}