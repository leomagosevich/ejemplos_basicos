package main

import ("fmt"
	"time")

func IsReady (what string, minutes time.Duration, invocacion int64) {
	fmt.Println("Invocación:", invocacion)
	time.Sleep (minutes * 60 * 1e9)
	fmt.Println (what, "está listo")
}

func main(){
	var input string
	go IsReady("Café", 2, 1)
	go IsReady("Te", 1, 2)
	fmt.Println("Estoy esperando...")
	fmt.Scanln(&input)
	fmt.Println("Terminó")
}