package main

import (
	"fmt"
	"sync"
	"time"
)

var (
	contador = 0
	lock     sync.Mutex
)

func main() {
	for i := 0; i < 3; i++ {
		go incremento()
	}
	time.Sleep(time.Millisecond * 10)
	fmt.Println("Finaliza")
}

func incremento() {
	lock.Lock()
	defer lock.Unlock()
	contador++
	fmt.Println("Cuenta:", contador)
}
