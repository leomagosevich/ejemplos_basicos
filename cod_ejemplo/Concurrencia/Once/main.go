package main

import (
	"fmt"
	"sync"
)

var (
	contador = 0
	lock     sync.Mutex
)

func main() {
	var ejecutafunc sync.Once
	for i := 0; i < 3; i++ {
		fmt.Println("Vuelta:", i+1)
		ejecutafunc.Do(incremento)
	}
	fmt.Println("Finaliza")
}

func incremento() {
	lock.Lock()
	defer lock.Unlock()
	contador++
	fmt.Println("Cuenta:", contador)
}
