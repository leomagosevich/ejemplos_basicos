package main

import (
	"fmt"
	"runtime"
	"sync"
	"time"
)

var wg sync.WaitGroup
var lineas []int
var lin []int

func imprimeSlice(lin []int, invocacion int) {
	defer wg.Done()
	var contador int
	for i := 0; i < len(lin); i++ {
		//fmt.Printf("Invocación %d - línea %d\n", invocacion, lin[i])
		//fmt.Printf("%d;%d\n", invocacion, lin[i])
		contador++
	}
}

func ObtenerFechaActual() string {
	t := time.Now()
	fecha := fmt.Sprintf("%d-%02d-%02d %02d:%02d:%02d", t.Year(), t.Month(), t.Day(), t.Hour(), t.Minute(), t.Second())

	return fecha
}

func main() {

	var corte int

	cpu := runtime.NumCPU()
	//cpu := 1

	runtime.GOMAXPROCS(cpu)

	fecha := ObtenerFechaActual()

	//corte := 8000012

	capacidad := 16000026

	//lineas := make([]int, 0, capacidad)

	fmt.Println("Prueba de Concurrencia con Slice")

	for i := 0; i < capacidad; i++ {
		lineas = append(lineas, i)
	}

	cantLineas := len(lineas)

	corte = cantLineas / cpu
	//cantProcesos := cantLineas / corte
	cantProcesos := cpu

	//cantResto := cantLineas % corte

	/*if cantResto > 0 {
		cantProcesos = cantProcesos + 1
	}*/

	wg.Add(cantProcesos)

	for i := 0; i < cantProcesos; i++ {

		switch i {
		case 0:
			lin = lineas[0:corte]
		case cantProcesos - 1:
			lin = lineas[corte*i : len(lineas)]
		default:
			lin = lineas[corte*i : corte*(i+1)]
		}

		go imprimeSlice(lin, i+1)
	}

	wg.Wait()

	fmt.Printf("Inicia Proceso %s\n", fecha)

	fmt.Printf("Cantidad de CPUs: %d\n", cpu)

	fmt.Println("Cantidad de líneas procesadas:", cantLineas)

	fmt.Printf("Procesos realizados: %d\n", cantProcesos)

	fmt.Printf("Valor de corte: %d\n", corte)

	fecha = ObtenerFechaActual()

	fmt.Printf("Finaliza proceso %s\n", fecha)
}
