package main

import (
	"fmt"
	"sync"
)

var wg sync.WaitGroup

func pump() chan []int {
	wg.Add(1)
	ch := make(chan []int, 100)
	lineas := []int{}
	go func() {
		defer wg.Done()
		for i := 0; i < 10; i++ {

			lineas = append(lineas, i)

		}
		fmt.Println("Imprimo el Slice")
		fmt.Println(lineas)
		ch <- lineas
	}()

	return ch
}

func main() {

	stream := pump()
	wg.Wait()
	fmt.Println("Imrpimo el slice que viajó por el canal retornado")
	fmt.Println(<-stream)
}
