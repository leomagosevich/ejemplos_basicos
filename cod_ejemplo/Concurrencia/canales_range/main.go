package main

import "fmt"

func pump() chan int{
	ch := make(chan int)
	go iter (ch)
	return ch
}

func iter(ch chan int){
	for i := 0; i<10 ; i++ {
		ch <- i
	}
}

func suck(ch chan int){
	go func (){
		for v:= range ch{
			fmt.Println(v)
		}
	}()
}

func main(){
	stream := pump()
	fmt.Println(<- stream)
	suck(pump())
}