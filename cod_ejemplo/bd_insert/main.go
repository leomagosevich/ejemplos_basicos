//Ejemplo de Insert a tabla en MariaDB
package main

import (
	"database/sql"
	"fmt"

	_ "github.com/go-sql-driver/mysql"
)

type Usuario struct {
	Legajo int
	Nombre string
	Edad   int
}

type UsuarioInsert struct {
	Nombre string
	Edad   int
}

func insertarUsuario(db sql.DB, u UsuarioInsert) error {

	insertaSQL, err := db.Prepare("INSERT INTO prueba (nombre, edad) VALUES (?, ?)")

	if err != nil {
		return err
	}

	defer insertaSQL.Close()

	_, err = insertaSQL.Exec(u.Nombre, u.Edad)

	if err != nil {
		return err
	}

	return nil

}

func obtenerUsuarios(db sql.DB) ([]Usuario, error) {

	usuarios := []Usuario{}

	filas, err := db.Query("SELECT legajo, nombre, edad FROM prueba")

	if err != nil {
		return nil, err
	}

	defer filas.Close()

	var u Usuario

	for filas.Next() {
		err := filas.Scan(&u.Legajo, &u.Nombre, &u.Edad)

		if err != nil {
			return nil, err
		}

		usuarios = append(usuarios, u)
	}

	return usuarios, nil

}

func conectarBD(usu string, pass string, base string) (db *sql.DB, e error) {

	db, err := sql.Open("mysql", fmt.Sprintf("%s:%s@/%s", usu, pass, base))

	if err != nil {
		return nil, err
	}

	return db, nil

}

func main() {

	var (
		usuario string
		passwd  string
		base    string
	)

	fmt.Printf("usuario BD: ")
	fmt.Scanf("%s", &usuario)
	fmt.Printf("Contraseña BD: ")
	fmt.Scanf("%s", &passwd)
	fmt.Printf("Base de Datos: ")
	fmt.Scanf("%s", &base)

	db, err := conectarBD(usuario, passwd, base)

	if err != nil {
		fmt.Printf("Error al conectar la BD: %v", err)
	}

	defer db.Close()

	usuarios, err := obtenerUsuarios(*db)

	if err != nil {
		fmt.Printf("Error al obtener listado de usuarios: %v", err)
	}

	for _, usuario := range usuarios {

		//Muestra por pantalla todos los campos seleccionados de la tabla
		fmt.Printf("%v\n", usuario)
	}

	var usuarioInsert UsuarioInsert

	fmt.Println("Ingresá los siguientes datos para agregar a un nuevo usuario")
	fmt.Printf("Nombre: ")
	fmt.Scanf("%s", &usuarioInsert.Nombre)
	fmt.Printf("Edad: ")
	fmt.Scanf("%d", &usuarioInsert.Edad)

	err = insertarUsuario(*db, usuarioInsert)

	if err != nil {
		fmt.Printf("Error al crear usuario: %v", err)
	}

	usuarios, err = obtenerUsuarios(*db)

	if err != nil {
		fmt.Printf("Error al obtener listado de usuarios: %v", err)
	}

	for _, usuario := range usuarios {

		//Muestra por pantalla todos los campos seleccionados de la tabla
		fmt.Printf("%v\n", usuario)
	}

}
