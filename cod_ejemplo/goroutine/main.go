package main

import (
	"fmt"
	"time")
	
func IsReady(what string, minutos int64)  {
	//var duracion time.Duration
	//duracion = minutos * 60 * 1e9
	fmt.Println(what, "pasa")
	time.Sleep(10 * time.Second)
	fmt.Println(what, "está listo")
}

func main() {
	go IsReady("El jugo", 2)
	go IsReady("La coca", 1)
	fmt.Println("Estoy preparando...")
}
