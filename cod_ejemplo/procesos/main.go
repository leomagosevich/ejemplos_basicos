package main

import (
	"bytes"
	"fmt"
	"os/exec"
)

func main() {

	prc := exec.Command("./hola", "Adios", "Abuela")
	out := bytes.NewBuffer([]byte{})
	prc.Stdout = out
	err := prc.Start()

	if err != nil {
		fmt.Println(err)
	}

	prc.Wait()

	if prc.ProcessState.Success() {
		fmt.Println("Proceso ejecutado exitosamente con salida:")
		fmt.Println(out.String())
	}
}
