package main

import (
	"fmt"
	"os"
)

func main(){
	saludo := os.Args[1]
	titulo := os.Args[2]
	fmt.Printf("%s %s (invocado desde otro proceso)\n", saludo, titulo)
}